module.exports = (sequelize, type) => {
    return sequelize.define('user', {
        name: {
        type: type.STRING,
        allowNull: false
        },
        surname: {
        type: type.STRING,
        allowNull: false
        },
        userID: { 
            type: type.STRING, 
            primaryKey: true 
        }
    }, { 
        timestamps: false
    });
}