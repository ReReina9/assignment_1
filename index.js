var express = require('express');
var app = express();

app.use(express.static(__dirname));

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());

let port = process.env.PORT || 8000;

module.exports = app.listen(port);
//app.listen(port);


var apiRoutes = require('./routes/api');


// apply the routes to our application with the prefix /api
app.use('/api', apiRoutes);
app.get('/', function (req, res) {
    res.render('index.html');
});




/*module.exports = {
    sayHello: function() {
        return 'hello';
    }
};
*/
